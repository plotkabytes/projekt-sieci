#include "logger.h"

const std::string Color::Logger::currentDateTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

    return buf;
}

void Color::Logger::printStatus(std::string data)
{
    Color::Logger blue(Color::FG_BLUE);

    std::cout   << blue
                << Color::Logger::currentDateTime()
                << "\t"
                << data
                << std::endl;
}

void Color::Logger::printInfo(std::string data)
{
    Color::Logger green(Color::FG_GREEN);

    std::cout   << green
                << Color::Logger::currentDateTime()
                << "\t"
                << data
                << std::endl;
}

void Color::Logger::printError(std::string data)
{
    Color::Logger red(Color::FG_RED);

    std::cout   << red
                << Color::Logger::currentDateTime()
                << "\t"
                << data
                << std::endl;

}
