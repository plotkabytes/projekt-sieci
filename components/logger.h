#ifndef LOGGER_H
#define LOGGER_H

#include <ostream>
#include <stdio.h>
#include <iostream>
#include <ctime>

namespace Color
{

enum Code {
    FG_RED      = 31,
    FG_GREEN    = 32,
    FG_BLUE     = 34,
    FG_DEFAULT  = 39,
    BG_RED      = 41,
    BG_GREEN    = 42,
    BG_BLUE     = 44,
    BG_DEFAULT  = 49
};

class Logger
{

Code code;

public:
    Logger(Code pCode) : code(pCode) {};
    Logger() {}

    friend std::ostream&
    operator<<(std::ostream& os, const Logger& mod) {
        return os << "\033[" << mod.code << "m";
    }

    void printStatus(std::string data);
    void printInfo(std::string data);
    void printError(std::string data);
    const std::string currentDateTime();

};

}

#endif // LOGGER_H

