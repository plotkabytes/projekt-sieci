#include "messagecontainer.h"

MessageContainer::MessageContainer()
{

}

MessageContainer::MessageContainer(MessageType messageType, char* content)
{
    this->messageType = messageType;
    this->content = content;
}

char *MessageContainer::serialize()
{
    QString serializedMessage;
    char* delimiter = (char*) ",";

    serializedMessage += QString(MESSAGE[this->messageType]);
    serializedMessage += delimiter;
    serializedMessage += QString(this->content);

    char *cstr = new char[serializedMessage.length() + 1];
    strcpy(cstr, serializedMessage.toStdString().c_str());

    return cstr;
}

void MessageContainer::deserialize(char* message)
{
    char* delimiter = (char*) ",";

    if(strstr(message,delimiter) == NULL)
    {
        this->messageType = WRONG_FORMAT;
        this->content = (char*) "WRONG_FORMAT";
    }
    else
    {
        QVector<char*> tableOfChars;
        QString tempContent;
        char* pch;

        pch = strtok (message,delimiter);
        while (pch != NULL)
        {
            tableOfChars.append(pch);
            pch = strtok (NULL,delimiter);
        }

        for(int i = 1; i < tableOfChars.size(); i++)
        {
            tempContent += tableOfChars[i];
            tempContent += delimiter;
        }

        tempContent[tempContent.length()-1] = 0;

        char *cstr = new char[tempContent.length() + 1];
        strcpy(cstr, tempContent.toStdString().c_str());

        this->messageType = (MessageType)atoi(tableOfChars[0]);
        this->content = cstr;
    }
}

MessageType MessageContainer::getMessageType() const
{
    return messageType;
}

void MessageContainer::setMessageType(const MessageType &value)
{
    messageType = value;
}

char *MessageContainer::getContent() const
{
    return content;
}

void MessageContainer::setContent(char *value)
{
    content = value;
}
