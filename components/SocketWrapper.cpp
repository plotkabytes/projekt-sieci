#include "SocketWrapper.h"

SocketWrapper::SocketWrapper()
{
    m_Sock = -1;
    id = std::rand();
}

bool SocketWrapper::Create()
{
    if((m_Sock = socket(AF_INET, SOCK_STREAM, 0)) > 0)
        return true;

    return false;
}

bool SocketWrapper::Bind(string address, int port)
{
    m_SockAddr.sin_family = AF_INET;
    m_SockAddr.sin_port = htons(port);
    m_SockAddr.sin_addr.s_addr = inet_addr(address.c_str());

    if(bind(m_Sock, (struct sockaddr *)&m_SockAddr, sizeof(struct sockaddr)) == 0)
        return true;

    return false;
}

bool SocketWrapper::Listen(int que)
{
    if(listen(m_Sock, que) == 0)
        return true;

    return false;
}

bool SocketWrapper::Accept(SocketWrapper &clientSock)
{
    int size = sizeof(struct sockaddr);

    clientSock.m_Sock = accept(m_Sock,(struct sockaddr *)&clientSock.m_SockAddr, (socklen_t *)&size);

    if(clientSock.m_Sock == -1)
        return false;

    return true;
}

bool SocketWrapper::Connect(string address, int port)
{
    struct in_addr *addr_ptr;
    struct hostent *hostPtr;
    string add;

    try
    {
        hostPtr = gethostbyname(address.c_str());

        if(hostPtr == NULL)
            return false;

        addr_ptr = (struct in_addr *)*hostPtr->h_addr_list;

        add = inet_ntoa(*addr_ptr);

        if(add == "")
            return false;
     }
     catch(int e)
     {
        return false;
     }

    struct sockaddr_in sockAddr;
    sockAddr.sin_family = AF_INET;
    sockAddr.sin_port = htons(port);
    sockAddr.sin_addr.s_addr = inet_addr(add.c_str());

    if(connect(m_Sock, (struct sockaddr *)&sockAddr, sizeof(struct sockaddr)) == 0)
        return true;

    return false;
}

int SocketWrapper::Recieve(char *buff, int buffLen)
{
    return recv(m_Sock, buff, buffLen, 0);
}

int SocketWrapper::Send(char *buff, int len)
{
    return send(m_Sock, buff, len, 0);
}

bool SocketWrapper::Close()
{
    if(m_Sock > 0)
    {
        close(m_Sock);

        m_Sock = 0;

        return true;
    }

    return false;
}

struct sockaddr_in SocketWrapper::get_m_SockAddr()
{
    return m_SockAddr;
}

int SocketWrapper::getId() const
{
    return id;
}

void SocketWrapper::setId(int value)
{
    id = value;
}
