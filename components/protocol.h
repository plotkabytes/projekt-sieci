#ifndef PROTOCOL
#define PROTOCOL

#define MESSAGE_SIZE 255

/*! Simple message used with MessageType enum */

const char* const MESSAGE[] =
{
    (char *) "0",
    (char *) "1",
    (char *) "2",
    (char *) "3",
    (char *) "4",
    (char *) "5",
    (char *) "6",
    (char *) "7",
    (char *) "8",
    (char *) "9",
    (char *) "10"
};

/*! Enum that is used to identify message */
enum MessageType
{
    WRONG_FORMAT,
    WRONG_ROOM,
    JOIN_SERWER,
    GET_ROOM_LIST,
    JOIN_ROOM,
    JOINED_ROOM,
    THROW_CARDS,
    NEXT_PLAYER_MOVE,
    GIVE_ME_CARDS,
    ECHO_MESSAGE,
    PLAYER_JOINED_ROOM
};

/*! Enum that represents type of the card */
enum Type
{
    DZIEWIEC,
    DZIESIEC,
    WALET,
    DAMA,
    KROL,
    AS
};

/*! Enum that represents color of the card */
enum CardColor
{
    PIK,
    KIER,
    KARO,
    TREFL
};

/*! Enum that represents position of the player - it is needed because we must know where player sits */
enum Place
{
    NONE,
    EAST,
    WEST,
    NORTH,
    SOUTH
};

/*! Enum that represents how many cards were thrown by player */
enum Throws
{
    ONE,
    TWO,
    THREE,
    FOUR,
    FIVE
};

#endif // PROTOCOL
