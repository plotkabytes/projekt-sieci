#ifndef MESSAGECONTAINER_H
#define MESSAGECONTAINER_H

#include <QString>
#include <QVector>
#include "protocol.h"

class MessageContainer
{

public:

    MessageContainer();
    MessageContainer(MessageType messageType, char* content);

    char* serialize();
    void deserialize(char* message);

    MessageType getMessageType() const;
    void setMessageType(const MessageType &value);

    char *getContent() const;
    void setContent(char *value);

private:

    MessageType messageType;
    char* content;

};

#endif // MESSAGECONTAINER_H
