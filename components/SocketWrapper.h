#ifndef SOCKETWRAPPER_H
#define SOCKETWRAPPER_H

#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <unistd.h>

using namespace std;

class SocketWrapper {

public:

    /*! Default constructor that initialize socket */
    SocketWrapper();

    /*! Function used to create socket
     * \return true if socket was created
     * \return false otherwise
     */
    bool Create();

    /*! Function used to bind socket to the adress and port
     * \return true if binding was successful
     * \return false otherwise
     */
    bool Bind(string address, int port);

    /*! Function used to listen for the clients
     * \param adress - address of the host
     * \param port - port of the host socket
     * \return true if listen == 0
     * \return false otherwise
     */
    bool Listen(int que);

    /*!
     * Function used to accept connection
     * \param clientSock - socket of the client
     * \return true if client was associated
     * \return false if client was not accepted
    */
    bool Accept(SocketWrapper &clientSock);

    /*!
     * Function used to connect to host
     * \param address - host adresss
     * \param port - host port
     * \return false if connection was not successful
     */
    bool Connect(string address, int port);

    /*!
     * Function used to receive message
     * \param buff - message to send
     * \param buffSize - sizeof message
     * \return value of recv
     */
    int Recieve(char *buff, int buffSize);

    /*!
     * Function used to send message
     * \param buff - message to send
     * \param len - sizeof message
     * \return value of send
     */
    int Send(char *buff, int len);

    /*!
     * Function used to close the socket
     * \return true if socket was successfully closed
     */
    bool Close();

    struct sockaddr_in get_m_SockAddr();

    int getId() const;

    void setId(int value);

private:

    int id;
    int m_Sock;
    struct sockaddr_in m_SockAddr;
};

#endif // SOCKETWRAPPER_H
