#include <QApplication>
#include "../components/logger.h"
#include "../components/SocketWrapper.h"
#include "UI/SerwerConnect.h"

#define SERWER_ADRESS "127.0.0.1"
#define SERWER_PORT 8888
#define MESSAGE_SIZE 255

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    SocketWrapper soc;

    SerwerConnect c(soc);

    c.show();

    return a.exec();
}
