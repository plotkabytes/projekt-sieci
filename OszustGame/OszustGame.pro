greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += main.cpp \
    ../components/logger.cpp \
    ../components/SocketWrapper.cpp \
    ../components/messagecontainer.cpp \
    Helpers/resourcemanager.cpp \
    UI/SerwerConnect.cpp \
    UI/RoomChoose.cpp \
    UI/GameWindow.cpp \
    Game/player.cpp \
    Game/myimage.cpp \
    Game/Card.cpp

HEADERS  += \
    ../components/protocol.h \
    ../components/logger.h \
    ../components/SocketWrapper.h \
    ../components/messagecontainer.h \
    Helpers/resourcemanager.h \
    UI/SerwerConnect.h \
    UI/RoomChoose.h \
    UI/GameWindow.h \
    Game/player.h \
    Game/myimage.h \
    Game/Card.h

FORMS    += \
    UI/SerwerConnect.ui \
    UI/RoomChoose.ui \
    UI/GameWindowForm.ui

RESOURCES += \
    resources.qrc

QT += core
QT += gui
QT += network

TARGET = OszustGame
TEMPLATE = app

CONFIG += \
    c++11 \
    -Wall

QMAKE_CXXFLAGS += -std=c++0x -pthread

LIBS += -pthread
