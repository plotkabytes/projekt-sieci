#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QGraphicsRectItem>
#include <stdio.h>
#include <iostream>
#include <pthread.h>
#include <QMessageBox>

#include "../OszustGame/Helpers/resourcemanager.h"
#include "../OszustGame/Game/myimage.h"
#include "../components/SocketWrapper.h"
#include "../components/logger.h"
#include "../components/protocol.h"
#include "../components/messagecontainer.h"

/*! Main form namespace */
namespace Ui {
class GameWindow;
}

/*! Class that is used to managing main window */
class GameWindow : public QMainWindow
{

public:

    /*! Singleton */
    static GameWindow* getInstance()
    {
        if(!instance)
            instance = new GameWindow();
        return instance;
    }

    /*! Default constructor */
    GameWindow();

    /*! Constructor with socket */
    GameWindow(SocketWrapper soc);

    /*! Default destructor */
    ~GameWindow();

    /*! Function that is only used to draw background of the widget */
    void DrawBackground();

    /*! Function used to load resources (actually images) from a files to memory */
    void loadResources();

    /*! Function used to unload(delete) resources (actually images) from memory */
    void unloadResources();

    /*! Function for reading messages from the serwer */
    static void *reader(void *instance);

    /*! Function for handling messages */
    static void *messageHandler(char *message);

    /*! Simple setter */
    void setSoc(const SocketWrapper &value);


private:

    static GameWindow* instance;

    bool                running;

    pthread_t           thread;
    SocketWrapper       soc;
    ResourceManager     resourceManager;

    Ui::GameWindow      *ui;
    QGraphicsScene      *scene;
    QGraphicsRectItem   *rectangle;

};

#endif // MAINWINDOW_H
