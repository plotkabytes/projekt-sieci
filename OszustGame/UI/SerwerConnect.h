#ifndef SERWERCONNECT_H
#define SERWERCONNECT_H

#include <QWidget>

#include "RoomChoose.h"

#include "../components/SocketWrapper.h"
#include "../components/logger.h"
#include "../components/protocol.h"
#include "../components/messagecontainer.h"


namespace Ui {
class SerwerConnect;
}

class SerwerConnect : public QWidget
{
    Q_OBJECT

public:

    explicit SerwerConnect(QWidget *parent = 0);
    SerwerConnect(SocketWrapper soc);
    ~SerwerConnect();

private slots:

    void on_Connect_clicked();

private:

    Ui::SerwerConnect *ui;
    SocketWrapper soc;

};

#endif // SERWERCONNECT_H
