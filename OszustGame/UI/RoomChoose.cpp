#include "RoomChoose.h"
#include "ui_RoomChoose.h"

RoomChoose::RoomChoose(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RoomChoose)
{
    ui->setupUi(this);
}

RoomChoose::RoomChoose(SocketWrapper soc) :
    ui(new Ui::RoomChoose)
{
    this->socket = soc;
    ui->setupUi(this);

    m = new MessageContainer();
    m->setMessageType(GET_ROOM_LIST);
    m->setContent( (char*) "GET_ROOM_LIST");

    char* serialized = m->serialize();
    char deserialized[MESSAGE_SIZE];

    socket.Send(serialized, MESSAGE_SIZE);
    socket.Recieve(deserialized,MESSAGE_SIZE);

    m->deserialize(deserialized);

    char* pch;
    char* delimiter = (char*) ",";

    pch = strtok(m->getContent(),delimiter);

    while (pch != NULL)
    {
        QListWidgetItem *newItem = new QListWidgetItem;
        newItem->setText(pch);
        ui->RoomList->addItem(newItem);
        pch = strtok (NULL,delimiter);
    }

}


RoomChoose::~RoomChoose()
{
    delete ui;
}

SocketWrapper RoomChoose::getSocket() const
{
    return socket;
}

void RoomChoose::setSocket(const SocketWrapper &value)
{
    socket = value;
}

void RoomChoose::on_ConnectBtn_clicked()
{
    QString chosenRoom;

    for(int row = 0; row < ui->RoomList->count(); row++)
    {
        QListWidgetItem *item = ui->RoomList->item(row);
        if(item->isSelected())
            chosenRoom = item->text();
    }

    char* choosenRoomToSend = strdup(chosenRoom.toStdString().c_str());

    m->setMessageType(JOIN_ROOM);
    m->setContent(choosenRoomToSend);

    char* serialized = m->serialize();
    socket.Send(serialized, MESSAGE_SIZE);

    char buff[MESSAGE_SIZE];
    socket.Recieve(buff,MESSAGE_SIZE);

    m->deserialize(buff);

    if(m->getMessageType() == JOINED_ROOM)
    {
        GameWindow *gameWindow = GameWindow::getInstance();
        gameWindow->setSoc(this->socket);
        gameWindow->show();

        this->close();
    }
    else
    {
        QErrorMessage error;
        error.showMessage("Nie udalo sie dolaczyc do pokoju, byc moze jest pelny a byc moze nie istnieje. Sprobuj ponownie pozniej");
    }

}
