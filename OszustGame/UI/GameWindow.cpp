#include "GameWindow.h"
#include "ui_GameWindowForm.h"

GameWindow* GameWindow::instance = NULL;

GameWindow::GameWindow() :
    ui(new Ui::GameWindow)
{
    running = true;

    loadResources();
    ui->setupUi(this);
    scene = new QGraphicsScene(this);
    DrawBackground();
    ui->Scena->setScene(scene);
    pthread_create(&thread, NULL, &GameWindow::reader,NULL);
}

GameWindow::GameWindow(SocketWrapper soc) :
    ui(new Ui::GameWindow)
{
    loadResources();
    ui->setupUi(this);
    scene = new QGraphicsScene(this);
    this->DrawBackground();
    ui->Scena->setScene(scene);
    this->soc = soc;
    pthread_create(&thread, NULL, &GameWindow::reader,NULL);
}

GameWindow::~GameWindow()
{
    unloadResources();
    running = false;
    delete ui;
}

void GameWindow::DrawBackground()
{
    QString fieldImage = ":/images/Background.jpg";

    scene->setSceneRect(0,0,0,0);
    ui->Scena->setRenderHints(QPainter::Antialiasing | QPainter::SmoothPixmapTransform);
    ui->Scena->setBackgroundBrush(QImage(fieldImage));
    ui->Scena->setCacheMode(QGraphicsView::CacheNone);

}

void GameWindow::loadResources()
{
    QVector<MyImage*> images = resourceManager.getImages();

    images.push_back(new MyImage(DZIEWIEC,TREFL,":/images/Cards/9Trefl.png"));
    images.push_back(new MyImage(DZIEWIEC,KARO,":/images/Cards/9Karo.png"));
    images.push_back(new MyImage(DZIEWIEC,KIER,":/images/Cards/9Kier.png"));
    images.push_back(new MyImage(DZIEWIEC,PIK,":/images/Cards/9Pik.png"));

    images.push_back(new MyImage(DZIESIEC,TREFL,":/images/Cards/10Trefl.png"));
    images.push_back(new MyImage(DZIESIEC,KARO,":/images/Cards/10Karo.png"));
    images.push_back(new MyImage(DZIESIEC,KIER,":/images/Cards/10Kier.png"));
    images.push_back(new MyImage(DZIESIEC,PIK,":/images/Cards/10Pik.png"));

    images.push_back(new MyImage(WALET,TREFL,":/images/Cards/WaletTrefl.png"));
    images.push_back(new MyImage(WALET,KARO,":/images/Cards/WaletKaro.png"));
    images.push_back(new MyImage(WALET,KIER,":/images/Cards/WaletKier.png"));
    images.push_back(new MyImage(WALET,PIK,":/images/Cards/WaletPik.png"));

    images.push_back(new MyImage(DAMA,TREFL,":/images/Cards/DamaTrefl.png"));
    images.push_back(new MyImage(DAMA,KARO,":/images/Cards/DamaKaro.png"));
    images.push_back(new MyImage(DAMA,KIER,":/images/Cards/DamaKier.png"));
    images.push_back(new MyImage(DAMA,PIK,":/images/Cards/DamaPik.png"));

    images.push_back(new MyImage(KROL,TREFL,":/images/Cards/KrolTrefl.png"));
    images.push_back(new MyImage(KROL,KARO,":/images/Cards/KrolKaro.png"));
    images.push_back(new MyImage(KROL,KIER,":/images/Cards/KrolKier.png"));
    images.push_back(new MyImage(KROL,PIK,":/images/Cards/KrolPik.png"));

    images.push_back(new MyImage(AS,TREFL,":/images/Cards/AsTrefl.png"));
    images.push_back(new MyImage(AS,KARO,":/images/Cards/AsKaro.png"));
    images.push_back(new MyImage(AS,KIER,":/images/Cards/AsKier.png"));
    images.push_back(new MyImage(AS,PIK,":/images/Cards/AsPik.png"));

    resourceManager.setImages(images);
}

void GameWindow::unloadResources()
{
    QVector<MyImage*> images = resourceManager.getImages();

    foreach (MyImage* img, images) {
        delete img;
    }
}

void* GameWindow::reader(void *)
{

    GameWindow* mainWindow = GameWindow::getInstance();
    char buff[MESSAGE_SIZE];

    while(mainWindow->running)
    {
        mainWindow->soc.Recieve(buff,MESSAGE_SIZE);
        mainWindow->messageHandler(buff);
    }

    return NULL;

}

void* GameWindow::messageHandler(char* message)
{
    GameWindow* mainWindow = GameWindow::getInstance();
    Ui::GameWindow* ui = mainWindow->ui;

    MessageContainer m;
    m.deserialize(message);

    if(m.getMessageType() == GET_ROOM_LIST)
    {
        //ui->Nick1Btn->setText("KURWA");
        return NULL;
    }

    return NULL;

}

void GameWindow::setSoc(const SocketWrapper &value)
{
    soc = value;
}
