#ifndef ROOMCHOOSE_H
#define ROOMCHOOSE_H

#include <QStringList>
#include <QWidget>
#include <QErrorMessage>

#include "GameWindow.h"

#include "../components/SocketWrapper.h"
#include "../components/messagecontainer.h"
#include "../components/logger.h"
#include "../components/protocol.h"

namespace Ui {
class RoomChoose;
}

class RoomChoose : public QWidget
{
    Q_OBJECT

public:

    explicit RoomChoose(QWidget *parent = 0);
    ~RoomChoose();
    RoomChoose(SocketWrapper soc);

    SocketWrapper getSocket() const;
    void setSocket(const SocketWrapper &value);

private slots:

    void on_ConnectBtn_clicked();

private:

    Ui::RoomChoose *ui;
    SocketWrapper socket;
    MessageContainer *m;

};

#endif // ROOMCHOOSE_H
