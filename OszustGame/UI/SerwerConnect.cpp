#include "SerwerConnect.h"
#include "ui_SerwerConnect.h"

SerwerConnect::SerwerConnect(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SerwerConnect)
{
    ui->setupUi(this);
}

SerwerConnect::SerwerConnect(SocketWrapper soc) :
    ui(new Ui::SerwerConnect)
{
    ui->setupUi(this);
    this->soc = soc;
}

SerwerConnect::~SerwerConnect()
{
    delete ui;
}

void SerwerConnect::on_Connect_clicked()
{
    QRegExp rxIP( "[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}" );
    QRegExp rxPORT( "[0-9]{1,5}" );

    QString ip = ui->IP->text();
    QString username = ui->Username->text();
    QString port = ui->Port->text();

    if(!rxIP.exactMatch(ip))
    {
        ui->StatusBar->setText("Wrong ip. ");
    }
    else if(username.isEmpty())
    {
        ui->StatusBar->setText("Wrong username");
    }
    else if(!rxPORT.exactMatch(port))
    {
        ui->StatusBar->setText("Wrong port. ");
    }
    else
    {
        ui->StatusBar->setText("Connecting... ");
        ui->Connect->setDisabled(true);
    }

    soc.Create();

    if(soc.Connect(ip.toStdString(),port.toInt()))
    {
        char buff[MESSAGE_SIZE];

        MessageContainer m;

        soc.Recieve(buff,MESSAGE_SIZE);
        m.deserialize(buff);

        ui->StatusBar->setText(m.getContent());
        ui->Connect->setDisabled(true);

        ui->StatusBar->setText("Socket ID: " + QString::number(soc.getId()));

        RoomChoose *choser = new RoomChoose(soc);
        choser->show();

        this->close();

    }
    else
    {
        ui->StatusBar->setText("Cannot connect. Check input data. ");
        ui->Connect->setDisabled(false);
    }

}
