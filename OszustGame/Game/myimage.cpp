#include "myimage.h"

MyImage::MyImage(Type t, CardColor c, QString p)
{
    this->type  = t;
    this->path  = p;
    this->color = c;
}

Type MyImage::getType()
{
    return this->type;
}

CardColor MyImage::getCardColor()
{
    return this->color;
}

QString MyImage::getPath()
{
    return this->path;
}
