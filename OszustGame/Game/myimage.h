#ifndef MYIMAGE_H
#define MYIMAGE_H
#include <QString>
#include "../components/protocol.h"

/*! Class that actually represents image */
class MyImage
{
public:

    /*! Default constructor
     * \sa MyImage(WALET,PIK,":/sciezka")
     * \param t - type of the card, available options: DZIEWIEC,DZIESIEC,WALET,DAMA,KROL,AS
     * \param c - CardColor of the card, available options: PIK,KIER,KARO,TREFL
     * \param p - path to the file in QString format
     */
    MyImage(Type t, CardColor c, QString p);

    /*! \return - type of the image */
    Type getType();

    /*! \return - CardColor of the image */
    CardColor getCardColor();

    /*! \return - path to the image file */
    QString getPath();

private:

    Type type;
    CardColor color;
    QString path;

};

#endif // MYIMAGE_H
