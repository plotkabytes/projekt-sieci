#ifndef CARDS_H
#define CARDS_H

#include <QImage>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMap>
#include "../components/protocol.h"

/*! Class that actually represents card */
class Card
{

public:

    /*! First type of constructor, allow to adjust all params
     * \sa Card(WALET,PIK,WALET_PIK_IMG)
     * \param t - type of the card, available options: DZIEWIEC,DZIESIEC,WALET,DAMA,KROL,AS
     * \param c - CardColor of the card, available options: PIK,KIER,KARO,TREFL
     * \param image - image of the card
     * \param h - is image hidden or not
     */
    Card(Type t, CardColor c, QImage image, bool h = false);

    /*! Second type of constructor, it will attach image using type and CardColor
     * \sa Card(WALET,PIK)
     * \param t - type of the card, available options: DZIEWIEC,DZIESIEC,WALET,DAMA,KROL,AS
     * \param c - CardColor of the card, available options: PIK,KIER,KARO,TREFL
     * \param h - is image hidden or not
     */
    Card(Type t, CardColor c, bool h = false);

    /*!
     * \sa drawCard(scene,view,posX,posY)
     * \param scene - scene of the QT widget
     * \param view - view of the QT widget
     * \param currentPosX - current x position of the image
     * \param currentPosY - current y position of the image
     */
    void    drawCard(QGraphicsScene scene, QGraphicsView view, qreal currentPosX, qreal currentPosY);

    /*!
     * \sa showCard(scene,view)
     * \param scene - scene of the QT widget
     * \param view - view of the QT widget
     */
    void    showCard(QGraphicsScene scene, QGraphicsView view);

    /*! \return type of the card */
    Type    getType();

    /*! \return image of the card */
    QImage  getImage();

    /*! \return CardColor of the card */
    CardColor   getCardColor();

    /*! \return if card is hidden - true else - false */
    bool    isHidden();

    /*! \return x position of the card image */
    qreal   getPosX();

    /*! \return y position of the card image */
    qreal   getPosY();

    /*! \param t - type of the card, available options: DZIEWIEC,DZIESIEC,WALET,DAMA,KROL,AS */
    void    setType(Type t);

    /*! \param img - image of the card */
    void    setImage(QImage img);

    /*! \param c - CardColor of the card, available options: PIK,KIER,KARO,TREFL */
    void    setCardColor(CardColor c);

    /*! \param t - true is card is hidden, false otherwise */
    void    setHidden(bool t);

    /*! \param x - x position of the image */
    void    setPosX(qreal x);

    /*! \param y - y position of the image */
    void    setPosY(qreal y);

private:

    Type        cardType;
    QImage      cardImage;
    CardColor   cardColor;

    bool    hidden;
    qreal   posX;
    qreal   posY;

};

#endif // CARDS_H
