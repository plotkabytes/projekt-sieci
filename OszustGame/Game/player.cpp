#include "player.h"

Player::Player(QString nickName, Place place, QVector<Card> cards)
{
    this->nick = nickName;
    this->whereAmI = place;
    this->cards = cards;
}

Player::Player(QString nickName, Place place)
{
    this->nick = nickName;
    this->whereAmI = place;
}

/*
void Player::throwCards(QVector<Card> selectedCards)
{
    foreach (Card card, selectedCards)
    {
        if(this->cards.contains(card))
            this->cards.remove(this->cards.indexOf(card));
    }

    //remove cards from player and send info about it
}

void Player::grabCards(QVector<Card> allTableCards)
{
    foreach(Card c, this->cards)
    {
        this->cards.push_back(c);
    }
}

void Player::takeCards(QVector<Card> threeCardsFromTheTable)
{

}
*/

void Player::setCards(QVector<Card> cards)
{
    this->cards = cards;
}

void Player::setNick(QString nickName)
{
    this->nick = nickName;
}

void Player::setPlace(Place place)
{
    this->whereAmI = place;
}

QString Player::getNick()
{
    return this->nick;
}

Place Player::getWhereAmI()
{
    return this->whereAmI;
}

QVector<Card> Player::getCards()
{
    return this->cards;
}
