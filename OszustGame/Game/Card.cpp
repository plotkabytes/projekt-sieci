#include "Card.h"

Card::Card(Type t, CardColor c, QImage image, bool h)
{
    this->cardImage = image;
    this->cardType =  t;
    this->cardColor = c;
    this->hidden = h;
}

Card::Card(Type t, CardColor c, bool h)
{
    this->cardType = t;
    this->cardColor = c;
    this->hidden = h;
    //this->cardImage = ResourceManager::loadImage(t,c);
}

Type Card::getType()
{
    return this->cardType;
}

QImage Card::getImage()
{
    return this->cardImage;
}

CardColor Card::getCardColor()
{
    return this->cardColor;
}

bool Card::isHidden()
{
    return this->hidden;
}

qreal Card::getPosX()
{
    return this->posX;
}

qreal Card::getPosY()
{
    return this->posY;
}

void Card::setCardColor(CardColor c)
{
    this->cardColor = c;
}

void Card::setType(Type t)
{
    this->cardType = t;
}

void Card::setImage(QImage img)
{
    this->cardImage = img;
}

void Card::setHidden(bool t)
{
    this->hidden = t;
}

void Card::setPosX(qreal x)
{
    this->posX = x;
}

void Card::setPosY(qreal y)
{
    this->posY = y;
}

void Card::drawCard(QGraphicsScene scene, QGraphicsView view, qreal currentPosX, qreal currentPosY)
{
    // if item doesnt exist
    if(currentPosX == 0.0 && currentPosY == 0.0)
    {
        //scene.addItem();

    }
    else
    {
        QGraphicsItem* currentItem = scene.itemAt(this->posX,this->posY,view.transform());
        scene.removeItem(currentItem);
        //scene.addItem();
    }
}

void Card::showCard(QGraphicsScene scene, QGraphicsView view)
{

}
