#ifndef PLAYER_H
#define PLAYER_H

#include <QVector>
#include <QtAlgorithms>
#include "Card.h"
#include "../components/protocol.h"

/*! Class that actually represents player */
class Player
{
public:

    /*!
     * First constructor - use only if you want create user WITH cards
     * \sa Player('Darek',WEST,cards)
     * \param nickName - nick of the user, it will be visible on the GUI
     * \param place - place where user sits, available options: NONE, EAST, WEST, NORTH, SOUTH
     * \param cards - cards which belongs to user
     */
    Player(QString nickName, Place place, QVector<Card> cards);

    /*!
     * Second constructor - use only if you want create user WITHOUT cards
     * \sa Player('Bartek',NORTH,cards)
     * \param nickName - nick of the user, it will be visible on the GUI
     * \param place - place where user sits, available options: NONE, EAST, WEST, NORTH, SOUTH
     */
    Player(QString nickName, Place place);

    /*!
     * Simple function that is used to throw (selected from GUI) cards to the table
     * \sa throwCards(selectedCards)
     * \param selectedCards
     */
    void throwCards(QVector<Card> selectedCards);

    /*!
     * Simple function that is used to grab all cards from the table, when you was cheating
     * \sa grabCards(cards)
     * \param allTableCards
     */
    void grabCards(QVector<Card> allTableCards);

    /*!
     * Simple function that is used to take three cards from the table when you dont want to cheat
     * \sa takeCards(cards)
     * \param threeCardsFromTheTable
     */
    void takeCards(QVector<Card> threeCardsFromTheTable);

    /*! \return - nick of the user */
    QString getNick();

    /*! \return - place where user sits */
    Place getWhereAmI();

    /*! \return - cards which belongs to user */
    QVector<Card> getCards();

    /*! \param cards - cards that should belong to the user */
    void setCards(QVector<Card> cards);

    /*! \param nickName - nick of the user */
    void setNick(QString nickName);

    /*! \param place - place where user sits */
    void setPlace(Place place);

private:

    Place           whereAmI;
    QString         nick;
    QVector<Card>   cards;

};

#endif // PLAYER_H
