#ifndef RESOURCEMANAGER_H
#define RESOURCEMANAGER_H

#include <QString>
#include <QImage>
#include <stdexcept>
#include "../components/protocol.h"
#include "../OszustGame/Game/myimage.h"

/*! Class that is used to load resources that represents images */
class ResourceManager
{

public:

    /*! Default constructor */
    ResourceManager();

    /*! Function that is used to get image from container by type and by color */
    QImage loadImage(Type t, CardColor c);

    /*! Function that is used to add image to the container */
    void setImages(QVector<MyImage *> vec);

    /*! Function that is used to get images container */
    QVector<MyImage*> getImages();

private:

    QVector<MyImage*> Images;

};

#endif // RESOURCEMANAGER_H
