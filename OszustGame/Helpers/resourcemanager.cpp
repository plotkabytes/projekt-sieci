#include "resourcemanager.h"

ResourceManager::ResourceManager()
{

}

QImage ResourceManager::loadImage(Type t, CardColor c)
{
    try{
        foreach(MyImage* img, this->Images)
        {
            if(img->getCardColor() == c && img->getType() == t)
            {
                return QImage(img->getPath());
            }
            else
            {
                throw std::invalid_argument("Error loading files, no image with that type and CardColor");
            }
        }
    }
    catch (const std::invalid_argument& e) {
        printf("%s",e.what());
        exit(1);
    }

    QImage NullResult;

    return NullResult;
}

void ResourceManager::setImages(QVector<MyImage*> vec)
{
    this->Images = vec;
}

QVector<MyImage*> ResourceManager::getImages()
{
    return this->Images;
}
