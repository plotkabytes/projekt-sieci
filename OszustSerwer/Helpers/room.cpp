#include "room.h"

Room::Room(QString roomName, int roomSize)
{
    this->roomName = roomName;
    this->roomSize = roomSize;
}

void Room::setRoomSize(int size)
{
    roomSize = size;
}

void Room::addPlayer(Player player)
{
    players.append(player);
}

void Room::setRoomName(QString name)
{
    roomName = name;
}

QString Room::getRoomName()
{
    return roomName;
}

QVector<Player> Room::getPlayers()
{
    return players;
}

void Room::setPlayers(QVector<Player> p)
{
    players = p;
}

void Room::sendToPlayers(char *message)
{
    for(int i = 0; i < players.size() - 1; i++)
    {
        Player currentPlayer = players[i];
        currentPlayer.socket.Send(message,MESSAGE_SIZE);
    }
}
