﻿#ifndef ROOM_H
#define ROOM_H

#include <QVector>
#include <iostream>
#include <QString>
#include "../components/SocketWrapper.h"
#include "../Projekt-Sieci/components/protocol.h"

struct Player
{
    int id;
    SocketWrapper socket;
    Place position;
};
class Room
{

public:

    Room() {}
    Room(QString roomName, int roomSize = 4);

    void setRoomSize(int size);
    void addPlayer(Player player);
    void setRoomName(QString name);
    void setPlayers(QVector<Player> p);
    void sendToPlayers(char* message);

    QString getRoomName();
    QVector<Player> getPlayers();

private:

    int roomSize;

    QString roomName;
    QVector<Player> players;

};

#endif // ROOM_H
