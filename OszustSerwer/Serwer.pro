greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

SOURCES += main.cpp \
    ../components/logger.cpp \
    ../components/SocketWrapper.cpp \
    ../components/messagecontainer.cpp \
    Helpers/room.cpp \
    Tests/serializationtest.cpp \
    Serwer/serwer.cpp

HEADERS += \
    ../components/protocol.h \
    ../components/logger.h \
    ../components/SocketWrapper.h \
    ../components/messagecontainer.h \
    Helpers/room.h \
    Serwer/serwer.h

QT += core
QT -= gui
QT += network

TARGET = Serwer

TEMPLATE = app

QMAKE_CXXFLAGS += -std=c++0x -pthread

QMAKE_CXXFLAGS_WARN_OFF = -Wunused-parameter

LIBS += -pthread

CONFIG += \
    c++11 \
    -Wall \
    console

CONFIG -= \
    app_bundle            
