#include "../Projekt-Sieci/components/messagecontainer.h"
#include "../Projekt-Sieci/components/protocol.h"
#include "../Projekt-Sieci/components/logger.h"

// in case of fire - change it to int main()
int mainTest()
{

    MessageContainer send;
    MessageContainer receive;
    Color::Logger log;

    char* content = (char*) "TEST,3";

    send.setMessageType(JOIN_ROOM);
    send.setContent(content);

    char* serialized = send.serialize();

    log.printError(serialized);

    receive.deserialize(serialized);

    log.printError("Message Type: " + std::to_string(receive.getMessageType()));
    log.printError(std::string(receive.getContent()));

    return 0;
}
