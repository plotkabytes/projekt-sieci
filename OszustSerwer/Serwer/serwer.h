#ifndef SERWER_H
#define SERWER_H

#include <QVector>
#include <stdio.h>
#include <pthread.h>

#include "../components/SocketWrapper.h"
#include "../Projekt-Sieci/components/logger.h"
#include "../Projekt-Sieci/components/protocol.h"
#include "../Projekt-Sieci/components/messagecontainer.h"

#include "../Helpers/room.h"

#define SERWER_ADRESS "127.0.0.1"
#define SERWER_PORT 8888
#define NUM_CLIENT 50

class Serwer {

public:

    Serwer();
    ~Serwer();

    void initSerwer();
    void initRooms();
    void start();

    static void *connectionHandler(void *clientSocket);
    static MessageContainer messageHandler(char *message, Serwer &serwerInstance, SocketWrapper &client);

private:

    SocketWrapper   socket;
    SocketWrapper   tempSocket;
    Color::Logger   log;
    QVector<Room>   rooms;
    pthread_t       thread;

};



#endif // SERWER_H
