﻿#include "serwer.h"

Serwer::Serwer()
{

}

Serwer::~Serwer()
{
    log.printStatus("Closing socket and exiting...");
    rooms.clear();
    socket.Close();
}

void Serwer::initRooms()
{
    rooms.reserve(20);

    Room testRoomFirst;
    testRoomFirst.setRoomName("Pokoj-1");
    testRoomFirst.setRoomSize(4);

    Room testRoomSecond;
    testRoomSecond.setRoomName("Pokoj-2");
    testRoomSecond.setRoomSize(4);

    rooms.append(testRoomFirst);
    rooms.append(testRoomSecond);
}

void Serwer::initSerwer()
{
    log.printInfo("********************************************\n\n "
                  "\t\t\t\t Oszust Serwer Status Info \n\n "
                  "\t\t\t********************************************\n");

    if(!socket.Create())
    {
        log.printError("Error creating socket!");
        log.printError("Exiting...");
    }
    else if(!socket.Bind(SERWER_ADRESS,SERWER_PORT))
    {
        log.printError("Error binding socket!");
        log.printError("Exiting...");
        socket.Close();
        exit(1);
    }
    else if(!socket.Listen(NUM_CLIENT))
    {
        log.printError("Error on listening!");
        log.printError("Exiting...");
        socket.Close();
        exit(1);
    }
    else
    {
        log.printInfo("Socket created...");
        log.printInfo("Socket binded...");
        log.printInfo("Waiting for connection...");
    }

}

void Serwer::start()
{
    initRooms();
    initSerwer();

    while(1)
    {

        SocketWrapper clientSocket;
        clientSocket.Create();

        if(!socket.Accept(clientSocket))
            log.printError("Error on accept...");
        else
        {
            log.printInfo("Got connection from: "
                          + std::string(inet_ntoa(clientSocket.get_m_SockAddr().sin_addr)));

            tempSocket = clientSocket;

            pthread_create(&thread, NULL, &Serwer::connectionHandler,(void*)this);
        }

    }

}

void* Serwer::connectionHandler(void* instance)
{
    char buff[MESSAGE_SIZE];

    Serwer serwerInstance = *(Serwer*) instance;

    SocketWrapper client = serwerInstance.tempSocket;
    Color::Logger log;

    MessageContainer m(JOIN_SERWER,(char*)"Connected ! ");
    char* serializedMsg = m.serialize();

    client.Send(serializedMsg,MESSAGE_SIZE);

    while(1)
    {
        if(client.Recieve(buff,MESSAGE_SIZE) > 0)
        {
            log.printStatus("Otrzymalem: " + std::string(buff));

            MessageContainer message = Serwer::messageHandler(buff,serwerInstance,client);
            serializedMsg = message.serialize();
            log.printInfo("Sending: " + std::string(serializedMsg));

            client.Send(serializedMsg,MESSAGE_SIZE);
        }
    }

    client.Close();

    return NULL;
}

MessageContainer Serwer::messageHandler(char* message, Serwer &serwerInstance, SocketWrapper &client)
{
    MessageContainer m;
    m.deserialize(message);

    if(m.getMessageType() == GET_ROOM_LIST)
    {
        QString tempSerwers;

        foreach(Room r, serwerInstance.rooms)
        {
            tempSerwers += r.getRoomName();
            tempSerwers += ',';
        }

        char *cstr = new char[tempSerwers.length() + 1];
        strcpy(cstr, tempSerwers.toStdString().c_str());

        m.setMessageType(GET_ROOM_LIST);
        m.setContent(cstr);

        return m;
    }
    else if(m.getMessageType() == JOIN_ROOM)
    {
        bool selected = false;

        for(int i = 0; i < serwerInstance.rooms.size(); i++)
        {
            Room r = serwerInstance.rooms[i];

            if(strcmp(r.getRoomName().toStdString().c_str(),m.getContent()) == 0)
            {
                Player p;
                p.position = WEST;
                p.socket = client;
                serwerInstance.rooms[i].addPlayer(p);

                selected = true;

                m.setMessageType(PLAYER_JOINED_ROOM);
                m.setContent((char*)"Player joined to room");
                char* serialziedMSG = m.serialize();
                r.sendToPlayers(serialziedMSG);

                m.setMessageType(JOINED_ROOM);
                m.setContent((char*) "JOINED_ROOM");

                return m;
            }
        }
        if(!selected)
        {
            m.setMessageType(WRONG_ROOM);
            m.setContent((char*) "WRONG_ROOM");

            return m;
        }
    }

    // if cannot resolve msg
    m.setMessageType(WRONG_FORMAT);
    m.setContent((char*) "WRONG_FORMAT");

    return m;
}
